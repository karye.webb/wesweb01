---
description: Hur man skickar data till en server.
---

# Formulär och PHP-backend

## Presentation

* [http://orion.lnu.se/pub/education/course/1IK424/VT14/sessions/F02.html#1](http://orion.lnu.se/pub/education/course/1IK424/VT14/sessions/F02.html#1)

## **Formulär i HTML**

![](<../.gitbook/assets/image (7).png>)

Ett program eller ett skript är inte särskilt meningsfullt om det inte kan fråga efter information i från användaren. I vanliga program matar användaren oftast in information från en kommandoprompt, eller vanligare nuförtiden, genom att fylla i en dialogruta eller välja ett menyalternativ. För att få in information från användaren till ett PHP-skript används formulär. Formulär ingår som en del i html-standarden

Processen går alltså till på detta sätt:

1. Användaren fyller i ett formulär på en webbsida (HTML-dokument).
2. Formulärinformationen skickas till PHP-skriptet på webbservern.
3. PHP-skriptet behandlar informationen.
4. PHP-skriptet skriver ut en webbsida som användaren får som respons.

### **Form-taggen omsluter formuläret**

Först ska vi titta på hur man utformar själva formuläret i html. Detta kan göras dels genom att koda för hand i ett redigeringsprogram som [VS Code](https://code.visualstudio.com). Vi kommer att skriva koden för hand. Det första man behöver i ett formulär är två stycken [form](https://devdocs.io/html/element/form)-taggar som omsluter själva formuläret:

```php
<form action="skript.php" method="post">
</form>
```

**Action** talar här om sökvägen till det PHP-skript som ska ta hand om formulärinnehållet. Med **method** specificerar vi med vilken metod formulärdata ska sändas iväg. Det finns två metoder: **get** och **post**. Används metoden **get** skickas informationen med i adressen till PHP-skriptet. Denna metod används ofta av sökmotorer. Det kan t.ex. se ut så här: Man använder vanligen i stället **post**, eftersom man med **get** inte kan skicka mer än c:a 250 tecken. Med **post**-metoden så paketeras informationen och skickas med m.h.a. http-protokollet. Detta har också den fördelen att användaren inte ser all teknisk information som skickas - ett sätt att "gömma" information som skickas mellan webbsidorna.

#### **\<input type="text"> ger en vanlig textruta**

Om vi matar in form-koden i föregående stycke, kommer ingenting alls att visas i webbläsaren. Vi behöver också ha rutor där användaren kan fylla i information. Ett vanligt textfält skrivs med [input](https://devdocs.io/html/element/input):

```php
<label>Ditt förnamn:
<input type="text" name="fornamn" value="skriv här" size="30">
</label>
```

Nästan alltid har man en beskrivning av formulärfältet till vänster, så att användaren vet vad som ska fyllas i. Sedan kommer själva html-taggen. **name** måste finnas med - detta för att php senare ska kunna särskilja just detta fält och dess innehåll. När formuläret skickas iväg kommer nämligen automatiskt en variabel skapas med namnet **$fornamn** som innehåller det användaren har fyllt i detta fält. Observera att man ej bör ha svenska bokstäver i **name**-attributet. **value** används oftast inte, men ger ett standardvärde i fältet. Till sist bestämmer vi hur många tecken brett formulärfältet ska vara med **size**.

#### **\<input type="password"> - skapar lösenordsruta**

Det här fältet fungerar precis som textrutan, med den skillnaden att det visas stjärnor när man fyller i fältet. Observera att informationen skickas helt i klartext till servern så ska man tillhandahålla känslig information med access via ett formulär är det lämpligt att skaffa en server med krypteringsfunktion.

Så här ser taggen ut:

```php
<label>Lösenord:
<input type="password" name="losenord">
</label>
```

#### **\<input type="checkbox"> - ger en kryssruta**

Det här fältet fungerar precis som kryssrutorna i t.ex. Windows - de kan antingen vara ikryssade eller ej ikryssade. Ett exempel:

```php
<label>Vilka semesterorter har du besökt<br>
<input name="azorerna" type="checkbox" value="1"> Azorerna<br>
<input name="mallorca" type="checkbox" value="1"> Mallorca<br>
<input name="teneriffa" type="checkbox" value="1"> Teneriffa
</label>
```

**name** talar om vilket namn variabeln kommer att få. Har användaren t.ex. kryssat i rutorna för Azorerna och Mallorca, kommer variabeln **$azorerna** få värdet 1, **$mallorca** också värdet 1, medan variabeln **$teneriffa** kommer att vara tom.

#### **\<input type="radio"> - skapa radioknappar**

Det finns ytterligare en typ av knapp, nämligen radioknappar. Ni känner säkert igen denna från Windows också. Den används när man ska välja ett, och endast ett alternativ av flera i en grupp. T.ex. om man behöver skriva in åldern – en person kan ju inte gärna ha två åldrar samtidigt! Vi kan ju tänka oss följande exempel vid beställning av en färdbiljett:

```php
<label>Hur gammal är du?<br>
<input name="alder" type="radio" value="barn"> Under 16 år<br>
<input name="alder" type="radio" value="ungdom"> 16 - 25 år<br>
<input name="alder" type="radio" value="vuxen"> Över 25 år
</label>
```

[input](https://devdocs.io/html/element/input)-taggen i exemplet har vi med för att få ny rad. Observera att det här är viktigt att alla radioknappar i samma grupp (de man ska välja bland) har samma **name**-attribut. Hade vi gett dem olika namn hade vi kunnat kryssa i alla rutorna, vilket inte hade varit så bra. I stället särskiljer vi respektive fält med olika värden i **value**. Om vi i exemplet fyllt i vuxen, så kommer PHP-skriptet att ta emot en variabel med namnet\*\* \*\*$alder och innehållet "vuxen".

#### **\<textarea> - flerradiga textfält**

Flerradiga textfält används bland annat i webbmailtjänster som t.ex. gmail.com för att skriva in meddelandetexten. Då används [textarea](https://devdocs.io/html/element/textarea)-fältet i HTML-koden.

```php
<label>Ange meddelande
<textarea name="meddelande"></textarea>
</label>
```

### **Andra formulärfält**

Det finns några till, mer ovanliga formulärfält, bland annat "dropdown"-menyer.

Till sist behöver vi en knapp som skickar iväg formulärinnehållet till servern. Detta gör vi med en submit-knapp.

```php
<button>Skicka</button>
```

### **Komplett formulärexempel**

Så är det dags att binda ihop allting och skapa ett komplett formulär. Vi börjar med ett enkelt exempel:

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Uppgift 1.2</title>
    <link rel="stylesheet" href="https://cdn.rawgit.com/Chalarangelo/mini.css/v3.0.1/dist/mini-default.min.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h2>Ålderstest</h2>
        <form action="backend.php" method="post">
            <label>Hur gammal är du?</label>
            <input type="text" name="alder" placeholder="Ange ett tal" required>
            <button class="primary">Skicka</button>
        </form>
    </div>
</body>
</html>
```

![](<../.gitbook/assets/image (111) (1) (1).png>)

För att ta emot formulärinnehållet behöver vi också ett PHP-skript i "andra änden". Det kan till exempel se ut så här:

```php
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Uppgift 1.2</title>
    <link rel="stylesheet" href="https://cdn.rawgit.com/Chalarangelo/mini.css/v3.0.1/dist/mini-default.min.css" />
    <link rel="stylesheet" href="style.css" />
</head>

<body>
    <div class="kontainer">
        <h2>Svar</h2>
        <?php
        // Ta emot data från formuläret
        $alder = filter_input(INPUT_POST, "alder");

        // Om data inte är tomt
        if ($alder) {
            echo "<p>Vänta medan priset för din $alder:års biljett räknas ut.</p>";
        }
        ?>
    </div>
</body>

</html>
```

![](<../.gitbook/assets/image (110) (1) (1).png>)

Alla formulärdata som skickats avläses med **filter\_input(INPUT\_POST, ...)**.

{% hint style="info" %}
Från PHP-kompendiet av Thomas Höjemo, © SNT 2006, www.snt.se\
[http://www.snt.se/phpkompendium.pdf](http://www.snt.se/phpkompendium.pdf)\
[http://www.snt.se/phpfacit.pdf](http://www.snt.se/phpfacit.pdf)
{% endhint %}

## Uppgifter

Observera att dessa övningar endast innefattar HTML, i nästa övningsmoment ska du bygga på övningarna med kod i PHP som tar emot formulärinnehållet. Kod för formulär-element hittar du här: [https://developer.mozilla.org/en-US/docs/Learn/Forms/Basic\_native\_form\_controls](https://developer.mozilla.org/en-US/docs/Learn/Forms/Basic\_native\_form\_controls)

Använd följande CSS:

```css
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}

body {
    background: #F9F6EB;
}
.kontainer {
    width: 600px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Open Sans', sans-serif;
    border: 1px solid #ddd;
    box-shadow: 0 0 12px #f0e9d1;
    color: #4e4e4e;
}
.kol2 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 1em;
}
.kol3 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr 1fr;
    grid-gap: 1em;
}
form {
    margin: 1em 0;
    padding: 1em;
    color: #4e4e4e;
    display: grid;
    grid-template-columns: 1fr 2fr;
    grid-gap: 1em;
}
label {
    font-size: 0.9em;
}
input, textarea {
    padding: 0.7em;
    font-style: italic;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
textarea {
    height: 10em;
}
button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
}
h1, h2, h3 {
    color: #9c813d;
}
h1, h2, h3, p {
    margin: 0.5em 0;
}
h3 {
    margin-top: 2em;
}

table {
    width: 100%;
    border-collapse: collapse;
    margin: 2em 0;
}
th, td {
    padding: 0.5em;
    text-align: left;
    background: none;
}
th {
    background: #305A85;
    color: #FFF;
}
tr:nth-child(even) {
    background: #E6F2F8;
}
tr:nth-child(odd) {
    background: #FFF;
}
table .fa {
    color: #55a5d2;
}
table img {
    width: 50px;
}
form img {
    width: 30px;
}
form span {
    padding: 10px;
}
```

### **Frontend**

#### **Uppgift 1**

Gör ett formulär där användaren kan mata in två tal i två stycken **textrutor**.

![](<../.gitbook/assets/image (30).png>)

#### **Uppgift 2**

Gör ett komplett "kontaktformulär" med textrutor där du frågar efter **namn**, **epostadress** och **telefon**. Du ska även skapa en **kryssruta** där användaren kan bocka i sitt intresse för ett nyhetsbrev. Till sist ska du skapa två stycken **radioknappar**, där användaren kan välja mellan att bli kontaktad per telefon eller e-post.\
I mottagande webbsida skrivs **namn**, **epost** och **telefon** ut.

![](<../.gitbook/assets/image (29).png>)

#### **Uppgift 3**

Skapa en sida där besökaren ska välja sin favoritfilm av tre olika filmer. Besökaren ska dessutom välja sin favoritbok bland tre olika böcker. Det ska gå att välja högst en bok och en film.\
I mottagande webbsida skrivs all skickad information ut.

![](<../.gitbook/assets/image (31).png>)

#### **Uppgift 4**

Utforma ett formulär på en sida där användaren ska mata in en temperatur i Celsius.

![](<../.gitbook/assets/image (24).png>)

#### **Uppgift 5**

På din webbsida vill du ha ett formulär där besökaren kan lämna kommentarer. Kommentarerna ska kunna skrivas i ett **flerradstextfält**. Dessutom ska det finnas en vanlig **textruta** för att fylla i namn. På knappen för att skicka iväg formuläret ska det stå "Skicka kommentarer".

![](<../.gitbook/assets/image (28).png>)

### **Backend**

#### **Uppgift 6.1**

Använd formuläret från [uppgift 1](html-och-formulaer.md#uppgift-1).

Skapa ett **backend**-sida som tar emot de två talen, summerar dem och presenterar resultatet så här: **"Summan av tal1 och tal2 är ..."**.

![](<../.gitbook/assets/image (109).png>)

#### **Uppgift 6.2**

Ändra formuläret från [uppgift 1](html-och-formulaer.md#uppgift-1) så det handlar om multiplikation.

Skapa ett **backend**-sida som tar emot de två talen, multiplicerar dem och presenterar resultatet så här: **"Produkten av tal1 och tal2 är ..."**.

![](../.gitbook/assets/dump-uppgift-2-1.png)

#### **Uppgift 7**

Använd formuläret från [uppgift 2](html-och-formulaer.md#uppgift-2).&#x20;

Skapa ett skript som tar emot data från detta formulär: Skriptet ska skriva ut **"Namn:"** följt av namnet på personen, **"epostadress:"** och personens epostadress och till sist **"Vi kommer att kontakta dig inom snarast per "** följt av antingen **epost** eller **telefon** beroende på vad användaren valt.

![](<../.gitbook/assets/image (112) (1).png>)

#### **Uppgift 8**

Gör ett formulär där användaren kan välja bland fyra färger att "måla" bakgrunden med för webbsidan som kommer upp när han/hon klickat på knappen **Måla bakgrund**. Skapa sedan ett skript som verkligen gör detta. Färgerna ska vara **röd**, **blå**, **grön** och **gul**.

Tips: färgerna skrivs med sina engelska namn enligt följande exempel (som visar en sida med svart bakgrund):

```php
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title>Bakgrundsfärg</title>
    </head>
    <body style="background: black;">
    </body>
</html>
```

![](../.gitbook/assets/dump-uppgift-2-3.png)

#### **Uppgift 9**

Skapa en webbsida som tar en siffra (från formuläret i [uppgift 4](html-och-formulaer.md#uppgift-4)) som innehåller dagens temperatur i Celsius.&#x20;

Programmet ska sedan skriva ut hur många grader Fahrenheit det motsvarar enligt följande mall: "**100 grader Celsius motsvarar 212 grader Fahrenheit**".&#x20;

Formeln för omvandlingen är **F = (9/5)\*C + 32** där F står för grader Fahrenheit och C för grader Celsius.

![](../.gitbook/assets/dump-uppgift-2-4.png)

#### **Uppgift 10**

Ändra föregående formulär så att man kan med radioknappar välja mellan "**Omvandla från F° till C°**" eller "**Omvandla C° till F°**". Ändra på skriptet så att uträkningen stämmer.

![](../.gitbook/assets/dump-uppgift-2-5.png)

#### **Uppgift 11**

Utöka föregående formulär så att man kan också välja "**Omvandla C° till K°**". K står för temperatur i Kelvin enligt formeln **K = C - 273**. Ändra på skriptet så att uträkningen stämmer.

#### **Uppgift 12**

Skapa ett formulär som tar emot en text och där man kan välja mellan att konvertera till **versaler** (stora bokstäver) eller till **gemener** (små bokstäver).&#x20;

Läs om funktioner på [strtoupper()](http://php.net/manual/en/function.strtoupper.php) och [strtolower()](http://php.net/manual/en/function.strtolower.php).

![](../.gitbook/assets/dump-uppgift-2-7.png)

#### **Uppgift 13**

Försök att lista ut vad följande program kommer att skriva ut.

```php
<?php
$ett = '$noll';
echo "<p>$ett</p>";
echo '<p>$ett</p>';
?>
```

Prova sedan att mata in koden och se om du gissade rätt.

### Extra uppgifter

#### Uppgift 14

Skapa en frontend-sida som frågar användaren vilket **årtal** det är.\
Skapa sedan en backend-sida som berättar hur många år det är kvar till år **2100**.

#### Uppgift 15

Skapa en frontend-sida som frågar användaren vilket hur **långt** hen kan hoppa mätt i meter.\
Skapa sedan en backend-sida som berättar hur mycket längre världsrekordet är (8,90 meter): \
"[**Bob Beamon**](https://sv.wikipedia.org/wiki/Bob\_Beamon) **hopar ... m längre än dig!**".

#### Uppgift 16

Skapa en frontend-sida som frågar användaren **två ord**.\
Skapa sedan en backend-sida som presentera en mening med orden i **omvänd** ordning.

#### Uppgift 17

Skriv en frontend-sida som frågar hur långt **Max** och **Jakob** hoppade.\
Skapa sedan en backend-sida som därefter skriver ut hur mycket längre som **Max** hoppade.

#### Uppgift 18

Skapa en frontend-sida för beräkna **kostnaden** för att hyra bil hos en biluthyrningsfirma.\
Startavgiften för att hyra bilen är **500:-**, därefter kostar det ytterligare **5:-/km** och **400:-** för varje extra dag förutom den första.\
Sidan ska fråga hur **många dagar** man vill hyra bilen och hur **många kilometer** man vill köra.\
Skapa sedan en backend-sida som ska sedan presentera den totala **hyran**.

#### Uppgift 19

Skapa en frontend-sida som ber användaren mata in lönen för **3** anställda.\
Skapa sedan en backend-sida som presentera **medellönen** för personalen.

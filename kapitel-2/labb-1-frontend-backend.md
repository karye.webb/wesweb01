# Labb 1 - frontend/backend

## Webbapp för att spara highscore

### Frontend formulär

![](<../.gitbook/assets/image (110) (1).png>)

#### Koden

```html
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Spara highscore</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="kontainer">
        <h1>Spara highscore</h1>
        <form action="backend.php" method="POST">
            <label for="namn">Ange namn</label>
            <input id="namn" class="form-control" type="text" name="namn" required>

            <label for="mobil">Ange mobil</label>
            <input id="mobil" class="form-control" type="text" name="mobil" required>

            <label for="hscore">Ange highscore</label>
            <input id="hscore" class="form-control" type="text" name="hscore" required>

            <button type="submit" class="btn btn-primary">Spara</button>
        </form>
    </div>
</body>

</html>
```

#### style.css

```css
@import url('https://fonts.googleapis.com/css2?family=Open+Sans&display=swap');
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
body {
    background: #F9F6EB;
}
.kontainer {
    width: 600px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Open Sans', sans-serif;
    border: 1px solid #ddd;
    box-shadow: 0 0 12px #f0e9d1;
    color: #4e4e4e;
}
h1, h2, h3 {
    color: #9c813d;
}
h1, h2, h3, p {
    margin: 0.5em 0;
}
h3 {
    margin-top: 2em;
}
form {
    margin: 1em 0;
    padding: 1em;
    font-size: 0.9em;
    color: #4e4e4e;
    background: #E6F2F8;
    border-radius: 0.3em;
}
form label {
    display: grid;
    grid-template-columns: 1fr 2fr;
    margin: 10px 0;
    padding: 0;
}
form input, form textarea {
    padding: 0.5em;
    margin-top: -0.4em;
    font-style: italic;
    border-radius: 0.3em;
    border: 2px solid #55a5d2;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
form textarea {
    height: 10em;
}
form button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
}
```

### Backend-sida

![](<../.gitbook/assets/image (111) (1).png>)

#### Koden

På backend-sidan har vi lagt in ett villkor för highscore så att inte "vilda" värden tas emot:\
$hscoren <= 0 || $hscoren > 1000

```php
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Spara highscore</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="kontainer">
        <h1>Resultatet</h1>
        <?php
        // Ta emot data från formuläret och spara i variabler
        $namnet = filter_input(INPUT_POST, "namn");
        $mobilen = filter_input(INPUT_POST, "mobil");
        $hscoren = filter_input(INPUT_POST, "hscore");

        // Testar
        //var_dump($namnet, $mobilen, $hscoren);

        // Kontroll att highscoren är korrekt
        if ($hscoren <= 0 || $hscoren > 1000) {
            // Skriv ut ett error
            echo "<p class=\"alert alert-danger\">Fel! Fuska inte! Highscore måste vara större än 0 och mindre än 1000.</p>";
        } else {
            
            // Skriv ut ett fint svar
            echo "<p>$namnet din highscore är $hscoren.</p>";
            echo "<p>$namnet din mobil är $mobilen.</p>";
            
            // Bekräfta att highscoren sparats
            echo "<p class=\"alert alert-success\">Din highscore har sparats</p>";
        }
        ?>
    </div>
</body>
</html>
```

### Uppgifter

Ändra frontend-formuläret så att användaren måste mata in:

* Förnamn
* Efternamn
* Epost
* Ålder

Ändra backend-sidan så att följande skrivs ut:

* Förnamn och efternamn
* Epost
* En varning om ålder är mindre än 5
* En varning om ålder är större än 100

## Webbapp för att spara meddelande

### Frontend formulär

![](<../.gitbook/assets/image (114) (1).png>)

#### Koden

```php
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Spara highscore</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="kontainer">
        <h1>Spara textmassa</h1>
        <p>Spara ned alla text i en fil "namn.txt"</p>
        <form action="backend2.php" method="POST">
            <label for="texten">Ange text</label>
            <textarea id="texten" class="form-control" name="texten" required></textarea>

            <label for="namnet">Ange namn</label>
            <input id="namnet" class="form-control" type="text" name="namnet" required>

            <button type="submit" class="btn btn-primary">Spara</button>
        </form>
    </div>
</body>

</html>
```

### Backend-sida

![](<../.gitbook/assets/image (110).png>)

#### Koden

```php
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Resultatet</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="kontainer">
        <h1>Resultatet</h1>
        <?php
        // Ta emot data från formuläret och spara i variabler
        $texten = filter_input(INPUT_POST, "texten");
        $namnet = filter_input(INPUT_POST, "namnet");

        // Kontroll att texten är korrekt
        if (strlen($texten) <= 5) {
            // Skriv ut ett error
            echo "<p class=\"alert alert-danger\">Fel! Din text är för kort.</p>";
        } else {
            // Bekräfta att texten sparats
            echo "<p class=\"alert alert-success\">Din texten är sparad i $namnet.txt:</p>";
            echo "<pre class=\"alert alert-info\">$texten</pre>";
        }
        ?>
    </div>
</body>
</html>
```

### Uppgifter

Ändra frontend-formuläret så att användaren måste mata in:

* Titel
* Url till en bild på Internet

Ändra backend-sidan så att följande skrivs ut:

* Titel
* Meddelandet
* Bild visas
* Varna om titel är kortare än 5 tecken

---
description: Variabler och kommentarer.
---

# Grunder i PHP

## **Att skriva PHP-kod**

Varje PHP-snutt börjar med **\<?php** och avslutas med **?>**. På detta sätt kan man väva in PHP-kod i vanliga HTML-dokument.

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Dagens datum</title>
</head>
<body>

</body>
</html>
```

Detta är ett helt vanligt HTML-dokument. Om vi tittar på det i en webbläsare kommer vi att få upp ett tomt dokument med titeln "Dagens datum". Nu kan vi väva in PHP-kod i dokumentet.

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Dagens datum</title>
</head>
<body>
    <?php
    echo date("Y-m-d H:i");
    ?>
</body>
</html>
```

Då får vi alltså automatiskt uträknat dagens datum för oss. Man kan även börja PHP-kod med endast **\<?**, det är en smaksak vilket man väljer. [echo](https://devdocs.io/php/function.echo) i koden betyder att vi ska skriva ut någonting på skärmen. Sedan använder vi [date()](https://devdocs.io/php/function.date) för att få dagens datum. **Y** talar om att vi vill ha årtalet (**Year**), **m** betyder **månad** och **d** dag i månaden. **H** står för **hour** vilket ger oss timslaget och i ger oss minuterna. Ett litet enklare exempel kan se ut så här:

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Dagens datum</title>
</head>
<body>
    <?php
    echo "<p>Hej världen!</p>";
    ?>
</body>
</html>
```

Detta skriver ut raden "Hej världen!" på skärmen. Så fort man ska skriva ut någonting så måste det inneslutas i citationstecken, också kallade dubbelfnuttar. Observera också att varje rad avslutas med ett semikolon. Man kan välja att skriva ut HTML-kod inuti [echo](https://devdocs.io/php/function.echo)-satsen också:

```php
<?php
echo "<h1>Hej världen</h1>";
?>
```

Nu kommer texten som skrivs ut på skärmen att vara en rubrik. Vi skulle kunna skriva in **h1**-taggen som gör texten till fetstil i själva HTML-koden i stället.

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Dagens datum</title>
</head>
<body>
    <h1>
    <?php
    echo "<p>Hej världen!</p>";
    ?>
    </h1>
</body>
</html>
```

Vilket ger samma resultat.

## **Variabler**

![Variabler är som kartonger!](<../.gitbook/assets/image (2).png>)

En variabel är en behållare för ett värde. Tänk dig den som en kökslåda som innehåller någonting. Vi har ett namn på kökslådan dvs. själva variabeln och sedan har variabeln ett innehåll. Det finns två huvudtyper av variabler: strängar och tal. Strängar innehåller text medan tal innehåller siffror. Innan man använder en variabel behöver den tilldelas ett värde. (jag har här uteslutit HTML-koden runt skriptet för att vi ska koncentrera oss på det viktiga).

```php
<?php
$namn = "Thomas Höjemo";
?>
```

Nu har variabeln **$namn** fått värdet "Thomas Höjemo". Observera att alla variabler alltid börjar med dollartecknet. Lika med tecknet talar om att variabeln ska få ett värde. När man ger en variabel ett strängvärde, vilket vi gör i detta fall, måste strängen vara omgiven av dubbelfnuttar. I annat fall kommer vi att få ett felmeddelande:

```php
<?php
$namn = Thomas Höjemo;
?>
```

**Parse error** betyder att PHP helt enkelt inte förstod vad vi menade. Man brukar använda variabelnamn som har små bokstäver. Det är också viktigt att använda variabelnamn som gör det enkelt att förstå vad variabeln ska användas till, så det är bättre att kalla en variabel för t.ex. **$efternamn** än **$var45**. Variabelnamnet får innehålla bokstäver, siffror samt understreck (\_) men kan ej börja med en siffra. Så hur använder man då variabler? Ofta vill man skriva ut variabelinnehållet på skärmen mixat med färdig text runt, och då gör man på följande sätt:

```php
<?php
$namn = "Thomas Höjemo";
echo "$namn";
?>
```

I detta fall kommer alltså variabelinnehållet att skrivas ut i stället för **$namn**. Observera att detta endast gäller om dubbla citationstecken används. Vi kan se vad som händer om vi har enkla citationstecken (**enkelfnuttar**) i stället:

```php
<?php
$namn = "Thomas Höjemo";
echo '$namn';
?>
```

Som ni ser skrivs **$namn** ordagrannt ut. Det är alltså viktigt att alltid använda dubbelfnuttar när man ska skriva ut variabler. Men det är ju tur att enkelfnuttar finns också, annars skulle det vara svårt att skriva ut dollartecken! Genom att använda punkttecknet kan man kombinera vanlig text och strängvariabler:

```php
<?php
$namn = "Thomas Höjemo";
echo "<p>Mitt namn är " . $namn . " och jag bor i Göteborg </p>";
?>
```

Vi kan även baka in variabeln mellan citationstecknen, men observera att detta endast går då det är mellanrum före och efter variabelnamnet:

```php
<?php
$namn = "Thomas Höjemo";
echo "<p>Mitt namn är $namn och jag bor i Göteborg </p>";
?>
```

För att använda tal i variabler så fungerar det på i stort sett samma sätt. Det är dock viktigt att komma ihåg att man aldrig ska ha några fnuttar runt talen. Observera också att man använder decimalpunkt, ej decimalkomma.

Ett exempel:

```php
<?php
$tal1 = 37.76;
$tal2 = 45.23;
$summa = $tal1 + $tal2;
echo "<p>Summan av $tal1 och $tal2 är $summa </p>";
?>
```

Man kan använda **+ - \*** och **/** för att räkna med tal. Dessa kallas med ett finare uttryck för aritmetiska operatorer. Det finns också något som kallas logiska operatorer men de kommer vi att gå igenom senare.

### **Datatyper**

PHP är ett löst typat språk. Detta innebär att man inte behöver deklarera vad för typ av variabel man vill ha innan man skapar den.

Det finns åtta olika typer av datatyper:

| Datatyp  | Beskrivning | Förklaring                                                        |
| -------- | ----------- | ----------------------------------------------------------------- |
| Boolean  | 1 eller 0   | sant eller falskt                                                 |
| Integer  | heltal      | positiva eller negativa heltal                                    |
| Float    | decimaltal  | positiva eller negativa decimaltal                                |
| String   | sträng      | kan innehålla både bokstäver och siffror                          |
| Objekt   | objekt      | används vid objektorienterad programmering                        |
| Array    | matris      | innehåller flera variabler som var och en kan ha olika datatyp    |
| Resource | resurs      | en pekare till en extern resurs, t.ex. en databas                 |
| Null     | ingenting   | en variabel som ej har satts till något värde har värdet **Null** |

#### **Typkonvertering**

PHP omvandlar tyst mellan olika variabeltyper:

```php
<?php
$tal1 = "7 kr";
$tal2 = "3 kr";
$summa = $tal1 + $tal2;
echo "<p>Summan är $summa </p>";
?>
```

I detta program sker typkonverteringen på raden där **$tal1** och **$tal2** adderas. Eftersom addition av förståeliga skäl är svårt att utföra med strängar, omvandlas variablerna **$tal1** och **$tal2** från strängar till tal. Slutresultatet blir att "Summan är 10" skrivs ut.

Det går även att explicit konvertera mellan olika typer. Detta görs genom att skriva typen man vill konvertera till enligt följande:

```php
<?php
$antal = 100.32;
$antal = (int) $antal;     // $antal innehåller heltalet 100
$antal = (double) $antal;  // $antal innehåller decimaltalet 100 (100.0)
$antal = (string) $antal;  // $antal innehåller strängen 100
?>
```

Detta kan bland annat vara användbart för att säkerställa att formulärdata verkligen är av rätt typ.

#### **Kontrollera variabeltyp**

Genom funktionen [gettype\*\*()\*\*](https://devdocs.io/php/function.gettype) kan man kontrollera vilken variabeltyp en viss variabel har.

```php
<?php
$variabel = "Hej";
echo gettype($variabel);   // Ger resultatet string
?>
```

För att undersöka om en variabel är av en viss typ används serien funktioner som börjar på is\_. T.ex. [is\_int()](https://devdocs.io/php/function.is-int), [is\_string()](https://devdocs.io/php/function.is-string), [is\_double()](https://devdocs.io/php/function.is-double).

```php
<?php
$variabel = "Hej";
if (is_string($variabel)) {
    echo "<p>Det var en sträng! </p>";
} else {
    echo "<p>Det var inte en sträng! </p>";
}
?>
```

### **Fördefinierade variabler**

PHP har en stor mängd fördefinierade variabler. Det vanligaste användningsområdet för dessa är för att se vilken information som skickats från ett formulär. När ett formulär skickats iväg till ett PHP-skript så lagras automatiskt formulärelementens innehåll i arrayen [$\_REQUEST](https://devdocs.io/php/reserved.variables.request), [$\_POST](https://devdocs.io/php/reserved.variables.post) eller [$\_GET](https://devdocs.io/php/reserved.variables.get). Mer information om matriser kommer senare i kursen.

Har vi tex skapat en textruta med namnet "adress" och användaren fyller i värdet "Karlsgatan 12" så kommer **$\_REQUEST\['adress']** få innehållet "Karlsgatan 12". Fyller användaren i stället i textrutan "telefon" med numret "012-345678" får **$\_REQUEST\['telefon']** innehållet "012-345678". För att skriva ut telefonnumret behövs endast [echo](https://devdocs.io/php/function.echo) enligt följande modell:

```php
echo $_POST['telefon'];
```

Vilket kommer att resultera i att "012-345678" skrivs ut på skärmen förutsatt att användaren matade in just det numret i formuläret.

I det fall en inställning ändrats i PHP-systemkonfigurationen så kommer även fristående variabler skapas. I exemplen ovan skulle i så fall variablerna **$adress** och **$telefon** skapas. Denna inställning är avstängd från början, eftersom det kan medföra säkerhetsrisker om den används på ett slarvigt sätt.

**$\_SERVER\['HTTP\_REFERER']** innehåller den sida som besökaren kom ifrån till skriptet. (Notera att refererer skrivs med ett r, vilket beror på en pinsam felstavning i http-protokollet.) **$\_SERVER\['REMOTE\_ADDR']** innehåller IP-adressen som besökaren har. **$\_SERVER\['PHP\_SELF']** innehåller den kompletta adressen till skriptet som körs. För mer information om alla fördefinierade variabler, se [PHP-manualen](http://www.php.net/manual).

## **Kommentarer**

Genom att lägga in kommentarer i sitt skript kan man skriva vad ett speciellt avsnitt gör. Har man långa komplicerade skript kan detta vara bra, eftersom det annars kan vara svårt att förstå vad varje del är till för. Det finns två olika sätt att skriva kommentarer i PHP. Det första sättet ser ut så här:

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Dagens datum</title>
</head>
<body>
    <?php 
    echo "<p>Hej världen! </p>";   // Skriver ut raden Hello World på skärmen
    ?>
</body>
</html>
```

// är avsett för kommentarer som sträcker sig över endast en rad. Med den andra typen av kommentarer kan man ha flera rader:

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Dagens datum</title>
</head>
<body>
    <?php 
    /*  Flera rader kommentarer..
        En rad till.. */
    echo "<p>Hej världen! </p>";
    ?>
</body>
</html>
```

Man börjar alltså kommentaren med **/\*** och avslutar med **\*/**.

## Svenskt språkstöd!

### Datum på svenska

[date()](https://devdocs.io/php/function.date) ger oss ett svar på engelska:

```php
<?php
$datum = date("l y F");
echo "<p>$datum</p>";
?>
```

### Byta språkstöd

För att få svar på svenska måste vi ange svenska **locale** med funktionen [setlocale()](https://devdocs.io/php/function.setlocale). Sen formaterar vi datumet mha [strftime()](https://devdocs.io/php/function.strftime):

```php
<?php
setlocale(LC_ALL, "sv_SE.utf8");
$dagensDatum = strftime("%A %y %B");
echo "<p>På svenska blir det: $dagensDatum</p>";
echo "<p>Man kan ju börja med stor bokstav: " . ucwords($dagensDatum) . "</p>";
?>
```

## Escape-tecken

Försöker man skriva ut tecknet " (**dubbelfnutt**) stöter man på problem eftersom den stänger strängen.\
För att lösa problemet används \ (**backslash**) framför. -tecknet kallas också för escape-tecken.

```php
<?php 
echo "<p>Mit smeknamn är \"Batman\"</p>";
?>
```

## Uppgifter

### Uppgift 1

I en PHP-sida skapa några variabler **$namn**, **$adress** och **$ålder.**\
Skriv ut "Mitt namn är ..., jag bor på ... och .. år.".

### Uppgift 2

I en PHP-sida skapa två variabler **$beskrivning** och **$url** för valfri webbsida på Internet.\
Skriv sedan ut "Webbsidan ... har adress ...".

### Uppgift 3

I en PHP-sida skapa två variabler **$githubNamn** och **$githubUrl** för dina repository på github.\
Skriv ut "Min repos på github heter ... och adress dit är ...".

### Uppgift 4

Gör en PHP-sida som skriver vad klocka är:\
Tex "Klockan är just nu 09:30".\
Kolla upp hur man använder [date()](https://devdocs.io/php/function.date).

### Uppgift 5

Skapa en PHP-sida som skriver ut ditt namn och när du fyller 25.

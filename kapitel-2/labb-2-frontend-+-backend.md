---
description: Istället fö två separata sidor, slår vi ihop frontend och backend
---

# Labb 2 - frontend + backend

## En webbshop

Vi bygger en webbshop åt [biltema.se](https://www.biltema.se), så att vi kan mata in deras varor:

![](<../.gitbook/assets/image (114).png>)

### Sidan shop.php

#### Steg 1 - frontend

När man fyller i formuläret ser det ut såhär:

![](<../.gitbook/assets/image (113).png>)

#### Steg 2 - backend

När man klickat på knappen och skickat blir svaret såhär:

![](<../.gitbook/assets/image (115).png>)

#### Koden till shop.php

Vi använder [Bootstrap ](https://getbootstrap.com/docs/5.1/getting-started/introduction/)för snygga till sidan.\
Fyll i det som saknas:

* Formuläret
* PHP-kod

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Biltema shop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1>Webbshop</h1>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="./shop.php">Lägg till</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="./kundvagn.php">Kundvagnen</a>
            </li>
        </ul>
        <form>
            ...
        </form>
        <?php
            // Ta emot data från formuläret
            //...

            // Får vi data från formuläret isåfall spara ned
            if ($namn && $pris && $artnr) {
            
                // Klockslag
                //$klockslag = ...
                
                // Spara ned i en textfil som heter kundvagn.txt
                //....
                
                // Meddela användaren att vi lyckats spara i kundvagnen
                //...;
            }
        ?>
    </div>
</body>
</html>
```

#### style.css

```css
.kontainer {
    padding: 4em;
    background: #f6f6f6;
    margin: auto 5em;
}
form {
    margin: 2em 0;
    padding: 1em;
}
button {
    margin-top: 2em;
}
```

### Sidan kundvagn.php

![](<../.gitbook/assets/image (117).png>)

#### Koden till kundvagn.php

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Biltema shop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1>Spara i kundvagnen</h1>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="./shop.php">Lägg till</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="./kundvagn.php">Kundvagnen</a>
            </li>
        </ul>
        <?php
        // Läs all text i filen
        ...

        // Skriv ut allt inuti taggen <pre></pre>
        ...
        ?>
    </div>
</body>
</html>
```

### Uppgifter

Ändra formuläret så att användaren måste mata in:

* Antal

Ändra svaret så att följande skrivs ut:

* Klockslag
* Vara
* Artikelnr
* Pris
* Total = antal \* pris

---
description: Installera alla verktyg, konfigurera alla inställningar, skapar alla mappar
---

# Utvecklingsmiljö

## Skapa utvecklingsmiljön

### Webbeditorn VS Code & tillägg

* Installera [VS Code](https://code.visualstudio.com)
* Installera [git-scm](https://git-scm.com)
* Installera tilläggen
  * [Beautify](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify)
  * [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)
  * [PHP Intelephense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client)
  * [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
  * [VSCode Great Icons](https://marketplace.visualstudio.com/items?itemName=emmanuelbeziat.vscode-great-icons)

![](../.gitbook/assets/image.png)

### Installera PHP och konfigurera VS Code

1. Ladda ned [PHP 7.4 x64 Thread Safe 2](https://windows.php.net/download)
2. Packa upp till c:/php
3. Öppna VS Code
4. Fyll i rätt sökväg se [https://code.visualstudio.com/docs/languages/php](https://code.visualstudio.com/docs/languages/php)

### Inställningar i VS Code

Öppna inställningar (Settings) och kontrollera att det står följande:

```php
"php.validate.executablePath": "c:/php/php.exe",
"emmet.includeLanguages": {
    "php": "html"
},
"editor.multiCursorModifier": "ctrlCmd",
"html.format.extraLiners": "",
"html.format.contentUnformatted": "pre,code,textarea",
"[json]": {
    "editor.defaultFormatter": "vscode.json-language-features"
},
"[php]": {
    "editor.defaultFormatter": "bmewburn.vscode-intelephense-client"
},
"[javascript]": {
    "editor.defaultFormatter": "vscode.typescript-language-features"
},
"[html]": {
    "editor.defaultFormatter": "HookyQR.beautify"
},
"editor.codeLens": false,
```

### Konfigurera github

* Skapa ett konto på [github.com](https://github.com)
* Skapa en mapp **c:/github/webbsrvpgm1**

![](<../.gitbook/assets/image (75).png>)

* Publicera mappen som **Public** på [github.com](https://github.com)

![](<../.gitbook/assets/image (76).png>)

## Installera Backend server

### Docker Desktop och LAMP-server

* Installera [Docker Desktop](https://docs.docker.com/docker-for-windows/install/)
* Gå in i BIOS och aktivera virtualisering
* I VS Code installera tillägget Docker
* Öppna terminalen i VS Code och kör kommandot:

```bash
# Om du använder powershell
docker run -d --restart unless-stopped -p 8080:80 -p 10000:10000 -v "c:\github\...:/var/www" -v mysql-data:/var/lib/mysql --name lamp karye/lampw

# Om du använder bash
docker run -d --restart unless-stopped -p 8080:80 -p 10000:10000 -v /host_mnt/c/github/...:/var/www -v mysql-data:/var/lib/mysql --name lamp karye/lampw
```

* Instruktioner till hur du använder LAMPW finns på [https://hub.docker.com/r/karye/lampw](https://hub.docker.com/r/karye/lampw)

---
description: En enkel inloggningen
---

# Labb 1 - inloggning

![](<../.gitbook/assets/image (100).png>)

## Frontend-sidan

### Formuläret

Vi skapar ett vanligt formulär.

![](<../.gitbook/assets/image (97).png>)

Använd [bootstrap](https://getbootstrap.com/docs/5.1/getting-started/introduction/) att för styla sidan:

{% tabs %}
{% tab title="formular.html" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inloggning</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1 class="display-4">Inloggning</h1>
        <form class="kol2" action="login.php" method="POST">
        ...
        </form>
    </div>
</body>
</html>
```
{% endtab %}

{% tab title="style.css" %}
```css
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap');

/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}

body {
    background: #F9F6EB;
}
.kontainer {
    width: 600px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Source Sans Pro', sans-serif;
    border: 1px solid #ddd;
    box-shadow: 0 0 12px #f0e9d1;
    color: #4e4e4e;
}
.kol2 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr;
    grid-gap: 1em;
}
.kol3 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr 1fr;
    grid-gap: 1em;
}
form {
    margin: 1em 0;
    color: #4e4e4e;
}
label {
    text-align: right;
    align-self: center;
    font-size: 0.9em;
}
input, textarea {
    padding: 0.7em;
    border-radius: 0.3em;
    border: 1px solid #ccc;
    font-weight: bold;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
textarea {
    height: 5em;
    width: 100%;
}
button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
}
h1, h2, h3 {
    color: #9c813d;
}
h1, h2, h3, p {
    margin: 0.5em 0;
}
h3 {
    margin-top: 2em;
}

table {
    width: 100%;
    border-collapse: collapse;
    margin: 2em 0;
}
th, td {
    padding: 0.5em;
    text-align: left;
}
th {
    background: #305A85;
    color: #FFF;
}
tr:nth-child(even) {
    background: #E6F2F8;
}
tr:nth-child(odd) {
    background: #FFF;
}
table .fa {
    color: #55a5d2;
}
table img {
    width: 50px;
}
form img {
    width: 30px;
}
form label {
    display: flex;
    align-items:center;
}
form span {
    padding: 10px;
}
```
{% endtab %}
{% endtabs %}

### Inmatningsfälten

```php
<label>Användarnamn</label>
<input type="text" name="anamn" placeholder="Tex erik12" required>
<label>Lösenord</label>
<input type="password" name="losen" required>
<button class="primary">Skicka</button>
```

## Backend-sidan

PHP-sidan tar emot data från formuläret och kontrollerar att login-uppgifterna stämmer.

{% tabs %}
{% tab title="login.php" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inloggning</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1 class="display-4">Inloggning</h1>
        <?php
        // Ta emot data som skickas
        ...

        // Kolla om användarnamn och lösenord stämmer
        ...

        ?>
    </div>
</body>
</html>
```
{% endtab %}
{% endtabs %}

### Ta emot data från frontend

För att ta emot data på ett säkert sätt används [filter-input](https://devdocs.io/php/function.filter-input).

```php
// Ta emot data som skickas
$anamn = filter_input(INPUT_POST, 'anamn');
$losen = filter_input(INPUT_POST, 'losen');
```

### Kontrollen

Nu är det dags att kontrollera det som matats in:

```php
// Kolla om användarnamn och lösenord stämmer
if ($anamn == "..." && $losen == "...") {
    
} else {
    
}
```

### Bootstrap alerts

Vi använder [bootstrap](https://getbootstrap.com/docs/5.1/components/alerts/) för att skriva ut snygga svar:

![](<../.gitbook/assets/image (99).png>)

Med en varningsklass blir det:

```php
echo "<p class=\"alert alert-success\">Du är inloggad!</p>";
```

Om inloggning blev fel kan det se ut såhär:

```php
echo "<p class=\"alert alert-warning\">Fel användarnamn eller lösenord. Vg försök igen!</a></p>";
```

## Inloggning med en sida

Istället för att skickas vidare till en annan PHP-sida kan man välja att skicka tillbaka till samma sida med **action="login.php"**:

{% tabs %}
{% tab title="login.php" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inloggning</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1 class="display-4">Inloggning</h1>
        <form class="kol2" action="login.php" method="POST">
            <label>Användarnamn</label>
            <input type="text" name="anamn" placeholder="Tex erik12" required>
            <label>Lösenord</label>
            <input type="password" name="losen" required>
            <button class="primary">Skicka</button>
        </form>
        <?php

        ?>
    </div>
</body>
</html>
```
{% endtab %}
{% endtabs %}

### Ta emot data

```php
// Ta emot data som skickas
$anamn = filter_input(INPUT_POST, 'anamn');
$losen = filter_input(INPUT_POST, 'losen');
...
```

Nu kollar vi först om data kommer:

```php
// Kommer formulärdata?
if ($anamn && $losen) {
...
}
```

Och till slut kan vi kontrollera om användarnamn och lösenord stämmer:

```php
// Kolla om användarnamn och lösenord stämmer
if ($anamn == "karim" && $losen == "ryde") {
    echo "<p class=\"alert alert-success\">Du är inloggad!</p>";
} else {
    echo "<p class=\"alert alert-warning\">Fel användarnamn eller lösenord. Vg försök igen! <br>
<a href=\"formular.html\"><< Tillbaka till inloggningen</a></p>";
}
```

## Uppgift - lucktext (mad libs)

![](<../.gitbook/assets/image (102).png>)

Skapa en digital lucktext. Hämta gärna inspiration från nätet, tex:

* [http://www.redkid.net/madlibs](http://www.redkid.net/madlibs/)
* [http://www.rinkworks.com/crazytales](http://www.rinkworks.com/crazytales/)

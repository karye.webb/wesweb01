# Table of contents

* [Introduktion](README.md)
* [Utvecklingsmiljö](utvecklingsmiljoe/README.md)
  * [PHP snippets](utvecklingsmiljoe/php-snippets.md)
* [Tutorials](tutorials.md)

## Klient & server <a href="#kapitel-1" id="kapitel-1"></a>

* [PHP på servern](kapitel-1/untitled.md)
* [Intro till Linux](kapitel-1/intro-till-linux.md)

## Intro till PHP <a href="#kapitel-2" id="kapitel-2"></a>

* [Grunder i PHP](kapitel-2/att-koda-php.md)
* [Formulär och PHP-backend](kapitel-2/html-och-formulaer.md)
* [Labb 1 - frontend/backend](kapitel-2/labb-1-frontend-backend.md)
* [Best Practice](kapitel-2/skicka-data-fran-formulaer.md)
* [Labb 2 - frontend + backend](kapitel-2/labb-2-frontend-+-backend.md)

## Villkor <a href="#kapitel-3" id="kapitel-3"></a>

* [if-satser och loopar](kapitel-3/ifsatser-och-loopar.md)
* [Labb 3 - inloggning](kapitel-3/inloggning.md)
* [Labb 4 - spara gästbok](kapitel-3/labb-2-lasa-gaestbok.md)

## Projekt

* [Installera Wordpress](projekt/installera-wordpress.md)
* [En dynamisk webbplats](projekt/en-dynamisk-webbplats.md)

## Filhantering <a href="#kapitel-5" id="kapitel-5"></a>

* [Läsa & skriva filer](kapitel-5/filhantering.md)
* [Labb 5 - läsa gästbok](kapitel-5/labb-3-laesa-gaestbok.md)
* [Labb 6 - läsa csv-fil](kapitel-5/skriva-ut-csv-fil.md)
* [Labb 7 - blogg](kapitel-5/skapa-en-enkel-blogg.md)

## Array & loopar <a href="#kapitel-4" id="kapitel-4"></a>

* [Arrayer och foreach-loop](kapitel-4/arrayer-och-foreach-loop.md)
* [Labb 8 - slumpa ordspråk](kapitel-4/slumpa-fram-ordsprak.md)
* [Labb 9 - omvandla till morse](kapitel-4/labb-omvandla-till-morse.md)
* [Projekt 1 - interaktiv berättelse](kapitel-4/projekt-interaktiv-beraettelse.md)

## Texthantering <a href="#kapitel-6" id="kapitel-6"></a>

* [Arbeta med text](kapitel-6/straenghantering.md)
* [Labb 10 - webscraping](kapitel-6/webscraping.md)
* [Labb 11 - läsa tsv-fil](kapitel-6/skriva-ut-tsv-fil.md)

## Hitta i text <a href="#kapitel-7" id="kapitel-7"></a>

* [Regex - reguljära uttryck](kapitel-7/regex-reguljaera-uttryck.md)

## Fördjupning <a href="#kapitel-8" id="kapitel-8"></a>

* [Skanna katalog](kapitel-8/skanna-kataloger.md)
* [Ladda upp filer](kapitel-8/ladda-upp-filer.md)
* [Säkerhet och inloggning](kapitel-8/saekerhet-och-inloggning.md)
* [Labb 12 - bildgalleri](kapitel-8/labb-7-bildgalleri.md)
* [Labb 13 - bygg-din-pc](kapitel-8/labb-7-webbshop.md)

## Använda databas <a href="#kapitel-9" id="kapitel-9"></a>

* [Grunder i SQL](kapitel-9/sql-grunder.md)
* [Ställa frågor](kapitel-9/sql-fragor.md)
* [Fritextsökning](kapitel-9/sql-fritext.md)
* [Relationer mellan tabeller](kapitel-9/sql-relationer.md)
* [Inbyggda funktioner](kapitel-9/sql-funktioner.md)
* [Lathund SQL](kapitel-9/lathund.md)
* [Labb - phpMyAdmin](kapitel-9/labb-phpmyadmin.md)
* [Labb 14 - blogg med databas](kapitel-9/labb-9-blogg-med-databas.md)

## Sessioner <a href="#kapitel-10" id="kapitel-10"></a>

* [Inloggning och sessioner](kapitel-10/loesenord-and-kryptering.md)
* [Inloggning i modal-fönster](kapitel-10/login-modal.md)

## Övrigt

* [Funktioner](oevrigt/funktioner.md)
* [Skicka epost](oevrigt/skicka-epost.md)
* [Hosting](oevrigt/hosting.md)
* [Felmeddelanden](oevrigt/felmeddelanden.md)

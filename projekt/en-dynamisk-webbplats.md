---
description: Bygg egen webbplats med dynamisk innehåll
---

# En dynamisk webbplats

* Enklare projekt med dynamiska webbsidor
* Skapa en webbplats med minst 3 webbsidor
* Inkludera en meny
* Använd gärna [bootstrap](https://getbootstrap.com/docs/5.1/getting-started/introduction/)
* Innehåller enkel PHP för:
  * visa [datum och tid](../kapitel-2/att-koda-php.md#datum-pa-svenska)
  * visa besökarens ip
  * inkludera filer
    * include "sidhuvud.php" eller "meny.php"
  * man kan byta språk
  * använder låst katalog med .htaccess

### Exempel på mer avancerade funktioner

* Webbplatsen kan innehålla:
  * en [besöksdagbok](../kapitel-3/labb-2-lasa-gaestbok.md)
  * ett [bildgalleri](../kapitel-8/labb-7-bildgalleri.md)
  * en sidan för att [ladda upp bilder](../kapitel-8/ladda-upp-filer.md)
  * en inloggning med [sessioner](../kapitel-10/loesenord-and-kryptering.md)

### Tips på andra funktioner

* [https://codeshack.io/21-useful-php-snippets/](https://codeshack.io/21-useful-php-snippets/)
* [https://css-tricks.com/snippets/php/](https://css-tricks.com/snippets/php/)
